import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ToDoItem} from "../../interfaces/todo";

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent {

  @Input() itemInput: ToDoItem;
  @Input() index: number;
  @Output() onRemove: EventEmitter<number> = new EventEmitter<number>();
  @Output() onEdit: EventEmitter<number> = new EventEmitter<number>();
  @Output() onChange: EventEmitter<number> = new EventEmitter<number>()

  constructor() {
    this.itemInput = {};
    this.index = 0
  }

  change(): void {
    this.onChange.emit(this.index)
  }

  remove(): void {
this.onRemove.emit(this.index)
  }

  edit(): void {
    this.onEdit.emit(this.index)
  }
}
