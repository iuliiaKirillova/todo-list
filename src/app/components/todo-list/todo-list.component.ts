import {Component,} from '@angular/core';
import {ToDoItem} from "../../interfaces/todo";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})


export class TodoListComponent {

  inputText: string = '';

  list: ToDoItem[] = [
    {title: "Покормить кота", done: false},
    {title: "Купить хлеб", done: false}
  ]

  addItem(): void {
    const title: string = this.inputText;
    const toDo = {title, done: false}

    this.list.push(toDo);
    this.inputText = '';
  }

  remove(ind: number) {
    this.list = this.list.filter((
      itemInput: ToDoItem, i: number) => ind !== i
    )
  }

  change(ind: number) {
    this.list[ind].done = !this.list[ind].done
  }


  edit(ind: number) {
    this.inputText = this.list[ind].title;
    const buttonEdit = document.querySelector(".edit");
    const buttonAdd = document.querySelector(".add");
    buttonEdit.removeAttribute("hidden");
    buttonAdd.setAttribute("hidden", "hidden");

    buttonEdit.addEventListener("click", () => {
        edit2();
        this.inputText = '';
      }
    )
    const edit2 = () => {
      this.list[ind].title = this.inputText;
      buttonAdd.removeAttribute("hidden");
      buttonEdit.setAttribute("hidden", "hidden");
    }

  }
}



